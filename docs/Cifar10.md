# Cifar10

## Projects

First create a project.

![Create New Project](res/create_new_project.png)

Select created project.

## Tags

Create a tags for the classes to train and predict.

![Create Tags](res/create_tags.png)

## Training

The page Image Classifier allows to train a model for images.

First you have to select a model from a list.
Here we select cifar10 as model.

![Model](res/select_model.png)

Then we select a dataset, here also cifar10.
The dataset will be downloaded before training.
We can also select dataset.yml which expects the file in the project folder,
which points to image files in the project folder for training.

![Dataset](res/select_dataset.png)

We have to select the class names by using tags.

![Classes](res/select_classes.png)

Start training.

## Files

After training we can see some files created in the project folder.

![Project Files](res/project_files.png)

There are downloaded training data, saved model and a config file.
The config.yml file contains information about classes and selected model.

```yaml
cifar10:
  batch_size: 100
  classes:
  - airplane
  - automobile
  - bird
  - cat
  - deer
  - dog
  - frog
  - horse
  - ship
  - truck
  dataset: cifar10
  epochs: 2
  img_height: 32
  img_width: 32
  learn_rate: 0.1
  model: cifar10
  modelfile: model.save
```

## Prediction

Upload a image file and make a prediction.

![Predict](res/predict.png)

The results shows a list of predictions with prediction value and class name.
The higher the prediction value, the more probable the image shows the class.
