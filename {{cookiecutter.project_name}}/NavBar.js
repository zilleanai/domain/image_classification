import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import classnames from 'classnames'

import { ROUTES } from 'routes'
import { storage } from 'comps/project'
import NavLink from './NavLink'

import './navbar.scss'


class NavBar extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      menuOpen: false,
      workflowOpen: false,
    }
  }

  componentWillReceiveProps() {
    this.setState({ menuOpen: false })
  }

  render() {
    const { isAuthenticated } = this.props
    const { menuOpen, workflowOpen } = this.state

    return (
      <nav className={classnames({ 'menu-open': menuOpen })}>
        <div className="container navbar-top">
          <NavLink exact to={ROUTES.Home} className="brand">
            ML.<span className="tld">Platform</span>({storage.getProject()})
          </NavLink>
          <a href="javascript:void(0);"
            className="burger"
            onClick={this.toggleResponsiveMenu}
          >
            Menu&nbsp;&nbsp;&#9776;
          </a>
          <div className="menu left">
            <NavLink to={ROUTES.Projects} />
            <NavLink to={ROUTES.Git} />
            <NavLink to={ROUTES.Tags} />
            <NavLink to={ROUTES.Files} params={ {subpath:'/'}}/>
            <NavLink to={ROUTES.ImageUpload} params={ { subpath: '/' }} />
            <NavLink to={ROUTES.ImageClassifier} />
            <a href="javascript:void(0);"
              onClick={this.toggleWorkflowMenu}
            >
              Workflow
            </a>
            <NavLink to={ROUTES.Contact} />
          </div>
	<div className="menu right">{workflowOpen ? this.renderWorkflowMenu() : null}</div>
        </div>
      </nav>
    )
  }

  toggleResponsiveMenu = () => {
    this.setState({ menuOpen: !this.state.menuOpen })
  }

  toggleWorkflowMenu = () => {
    this.setState({ workflowOpen: !this.state.workflowOpen })
  }

  renderWorkflowMenu() {
    return (
      <nav>
	<NavLink to={ROUTES.Workflow} />
        <NavLink to={ROUTES.Labeling} />
        <NavLink to={ROUTES.Preprocessing} />
        <NavLink to={ROUTES.Train} />
        <NavLink to={ROUTES.Predict} />
      </nav>
    )
  }

  renderAuthenticatedMenu() {
    return (
      <div>
        <NavLink to={ROUTES.Profile} />
        <NavLink to={ROUTES.Logout} />
      </div>
    )
  }

  renderUnauthenticatedMenu() {
    return (
      <div>
        <NavLink to={ROUTES.SignUp} />
        <NavLink to={ROUTES.Login} />
      </div>
    )
  }
}

const withConnect = connect(
  (state) => ({ isAuthenticated: state.security.isAuthenticated }),
)

export default compose(
  withRouter,  // required for NavLinks to determine whether they're active or not
  withConnect,
)(NavBar)


