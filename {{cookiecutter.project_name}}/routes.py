from flask_unchained import (controller, resource, func, include, prefix,
                             get, delete, post, patch, put, rule)

from .views import SiteController

routes = lambda: [
    include('bundles.project.routes'),
    include('bundles.tag.routes'),
    include('bundles.git.routes'),
    include('bundles.file.routes'),
    include('bundles.image.routes'),
    include('bundles.image_classifier.routes'),
    include('bundles.workflow.routes'),
    include('bundles.train.routes'),
    include('bundles.labeling.routes'),
    include('bundles.preprocessing.routes'),
    include('bundles.predict.routes'),
    controller(SiteController), 
]
