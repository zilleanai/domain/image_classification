# image_classification

image classification

## installation

```bash
pip install git+https://github.com/zilleanai/zillean_cli
zillean-cli domain install https://gitlab.com/zilleanai/domain/image_classification
```

## Example

[Cifar10](docs/Cifar10.md)
